
/*-------------------------------------------------------------
 *           UNIFAL – Universidade Federal de Alfenas.         
 *                                                                   
 * BACHARELADO EM CIENCIA DA COMPUTACAO.                        
 * Trabalho..: Afinamento homotópico                           
 * Disciplina: PROCESSAMENTO DE IMAGENS
 * Professor.: Luiz Eduardo da Silva
 * 
 * Aluno.....: Rodrigo de Oliveira
 *             Romário da Silva Borges
 *             Vinicius Alex da Silva
 *             Vinicius Nogueira da Silva
 * 
 * Data......: 24/06/2016
 *-------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TRUE  1
#define FALSE 0
#define CREATOR "# CREATOR: Luiz Eduardo V:1.00 R:MAR/2015\n"

typedef int * imagem;
typedef int Tmat[3][3];

/*+-------------------------------------------------------------------------+
  | Rotinas para ALOCAR e DESALOCAR dinamicamente espaco de memoria para um |
  | vetor monodimensional que armazenara' a imagem.                         |
  | PARAMETROS:                                                             |
  | I  = Endereco do vetor (ponteiro de inteiros).                          |
  | nl = Numero de linhas.                                                  |
  | nc = Numero de colunas.                                                 |
  +-------------------------------------------------------------------------+*/
int aloca_memo(imagem *I, int nl, int nc) {
    *I = (int *) malloc(nl * nc * sizeof (int));
    if (*I) return TRUE;
    else return FALSE;
}

int desaloca_memo(imagem *I) {
    free(*I);
}

/*+-------------------------------------------------------------------------+
  | Apresenta informacoes sobre um arquivo de imagem.                       |
  | Parametros:                                                             |
  |   nome = nome fisico do arquivo de imagem.                              |
  |   nl   = numero de linhas da imagem.                                    |
  |   nc   = numero de colunas da imagem.                                   |
  |   mn   = maximo nivel de cinza da imagem.                               |
  +-------------------------------------------------------------------------+*/
void info_imagem(char *nome, int nl, int nc, int mn) {
    printf("\nINFORMACOES SOBRE A IMAGEM:");
    printf("\n--------------------------\n");
    printf("Nome do arquivo.............: %s \n", nome);
    printf("Numero de linhas............: %d \n", nl);
    printf("Numero de colunas...........: %d \n", nc);
    printf("Maximo nivel-de-cinza/cores.: %d \n\n", mn);
}

/*+-------------------------------------------------------------------------+
  | Rotina que faz a leitura de uma imagem em formato .PGM ASCII e armazena |
  | num vetor monodimensional. Um exemplo de imagem .PGM ASCII gravada neste|
  | formato:                                                                |
  |                                                                         |
  | P2                                                                      |
  | # CREATOR: XV Version 3.10a  Rev: 12/29/94                              |
  | 124 122                                                                 |
  | 255                                                                     |
  | 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255     |
  | 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255     |
  | (...)                                                                   |
  |                                                                         |
  | Lin 1: contem P2, o que identifica este arquivo como PGM ASCII.         |
  | Lin 2: contem um comentario qualquer iniciado com #.                    |
  | Lin 3: numero de colunas e numero de linhas da imagem.                  |
  | Lin 4: maximo nivel de cinza na imagem (255 normalmente).               |
  | Lin 5 em diante: valores de cinza da imagem.                            |
  |                                                                         |
  | PARAMETROS:                                                             |
  | nome = nome do arquivo (entra).                                         |
  | I    = ponteiro para o vetor imagem (retorna).                          |
  | nl   = numero de linhas da imagem (retorna).                            |
  | nc   = numero de colunas da imagem (retorna).                           |
  | mn   = maximo nivel de cinza (retorna).                                 |
  +-------------------------------------------------------------------------+*/
int le_imagem_pgm(char *nome, imagem *I, int *nl, int *nc, int *mn) {
    int i, j, k, LIMX, LIMY, MAX_NIVEL;
    char c, LINHA[100];
    FILE *arq;
    if ((arq = fopen(nome, "r")) == NULL) {
        printf("Erro na ABERTURA do arquivo <%s>\n\n", nome);
        return FALSE;
    }
    /*-- PGM = "P2" -----------*/
    fgets(LINHA, 80, arq);
    if (!strstr(LINHA, "P2")) {
        printf("Erro no FORMATO do arquivo <%s>\n\n", nome);
        return FALSE;
    }
    /*-- Dimensoes da imagem --*/
    fgets(LINHA, 80, arq);
    do {
        fgets(LINHA, 80, arq);
    } while (strchr(LINHA, '#'));
    sscanf(LINHA, "%d %d", &LIMX, &LIMY);
    fscanf(arq, "%d", &MAX_NIVEL);

    if (LIMX == 0 || LIMY == 0 || MAX_NIVEL == 0) {
        printf("Erro nas DIMENSOES do arquivo <%s>\n\n", nome);
        return FALSE;
    }

    if (aloca_memo(I, LIMY, LIMX)) {
        for (i = 0; i < LIMY; i++) {
            for (j = 0; j < LIMX; j++) {
                fscanf(arq, "%d", &k);
                if (k > MAX_NIVEL) {
                    printf("Erro nos DADOS do arquivo <%s>\n", nome);
                    printf("Valor lido: %d   Max Nivel: %d\n\n", k, MAX_NIVEL);
                    return FALSE;
                }
                *(*(I) + i * LIMX + j) = k;
            }
        }
        fclose(arq);
    } else {
        printf("Erro na ALOCACAO DE MEMORIA para o arquivo <%s>\n\n", nome);
        printf("Rotina: le_imagem_pgm\n\n");
        fclose(arq);
        return FALSE;
    }
    *nc = LIMX;
    *nl = LIMY;
    *mn = MAX_NIVEL;
    return TRUE;
}

/*+-------------------------------------------------------------------------+
  | Rotina que grava o arquivo da imagem em formato PGM ASCII.              |
  | PARAMETROS:                                                             |
  | I    = ponteiro para o vetor imagem (entra).                            |
  | nome = nome do arquivo (entra).                                         |
  | nl   = numero de linhas (entra).                                        |
  | nc   = numero de colunas (entra).                                       |
  | mn   = maximo nivel de cinza (entra).                                   |
  +-------------------------------------------------------------------------+*/
void grava_imagem_pgm(imagem I, char *nome, int nl, int nc, int mn) {
    int i, j, x, k, valores_por_linha;
    FILE *arq;
    if ((arq = fopen(nome, "wt")) == NULL) {
        printf("Erro na CRIACAO do arquivo <%s>\n\n", nome);
    } else {
        fputs("P2\n", arq);
        fputs(CREATOR, arq);
        fprintf(arq, "%d  %d\n", nc, nl);
        fprintf(arq, "%d\n", mn);
        valores_por_linha = 16;
        for (i = 0, k = 0; i < nl; i++)
            for (j = 0; j < nc; j++) {
                x = *(I + i * nc + j);
                fprintf(arq, "%3d ", x);
                k++;
                if (k > valores_por_linha) {
                    fprintf(arq, "\n");
                    k = 0;
                }
            }
    }
    fclose(arq);
}

void salvaimg(imagem I, imagem O, int nl, int nc) {
    int i, j;
    for (i = 0; i < nl; i++) {
        for (j = 0; j < nc; j++) {
            O[i * nc + j] = I[i * nc + j];
        }
    }
}
void pondo_mascara(int *mask, int *maskAux){
    int m;
    for (m = 0; m < 36; m++) {
        mask[m] = maskAux[m];
    }   
}

void esqueletizacao(imagem I, imagem O, int nl, int nc, int mn, int mascara, int rotacao) {

    int verificar = 1, i, j, k, l, elemento=0, cont=0, aux=0, vezes=0, relevantes=5;
    int percorreu[8] = {0, 9, 18, 27, 36, 45, 54, 64}, mask[36], mask2[36];

    /*-------------------------Máscaras----------------------*/
    int M1_L2impar[36] = {2,255,2,0,0,0,2,0,2, 2,0,2,0,0,255,2,0,2, 2,0,2,0,0,0,2,255,2, 2,0,2,255,0,0,2,0,2};
    int M1_L2par[36] =   {0,2,255,2,0,2,0,2,0, 0,2,0,2,0,2,0,2,255, 0,2,0,2,0,2,255,2,0, 255,2,0,2,0,2,0,2,0};

    int M2impar[36] = {2,255,255,0,0,2,2,0,2, 2,0,2,0,0,255,2,2,255, 2,0,2,2,0,0,255,255,2, 255,2,2,255,0,0,2,0,2};
    int M2par[36] =   {0,2,255,2,0,255,0,2,2, 0,2,0,2,0,2,2,255,255, 2,2,0,255,0,2,255,2,0, 255,255,2,2,0,2,0,2,0};
    
    int L1impar[36] = {255,255,255,2,0,2,0,0,0, 0,2,255,0,0,255,0,2,255, 0,0,0,2,0,2,255,255,255, 255,2,0,255,0,0,255,2,0};
    int L1par[36]   = {2,255,255,0,0,255,0,0,2, 0,0,2,0,0,255,2,255,255, 2,0,0,255,0,0,255,255,2, 255,255,2,255,0,0,2,0,0};
    
    if (rotacao == 8) mascara = mascara + 4; 
    /*----------Alocação das mascaras no vetor Mask[] -----------*/
    switch (mascara) {
        case(1):
            pondo_mascara(mask, M1_L2impar);        
            break;

        case(2): 
            pondo_mascara(mask, M2impar);
            break;

        case(3):
            pondo_mascara(mask, L1impar);
            relevantes = 7; // Relevantes indica a quantidade de elementos relevantes da mascara, ou seja, exclui os asteriscos
            break;

        case(4):
            pondo_mascara(mask, M1_L2impar);    
            break;

        case(5): 
            pondo_mascara(mask, M1_L2impar);
            pondo_mascara(mask2, M1_L2par);    
            break;

        case(6):
            pondo_mascara(mask, M2impar);
            pondo_mascara(mask2, M2par);    
            break;

        case(7):
            pondo_mascara(mask, L1impar);
            pondo_mascara(mask2, L1par);    
            relevantes = 7; 
            break;

        case(8):
            pondo_mascara(mask, M1_L2impar);
            pondo_mascara(mask2, M1_L2par);                
            break;

        case(9):
            pondo_mascara(mask, M1_L2impar);
            pondo_mascara(mask2, M1_L2impar);    
            break;
            
        case(10):
            pondo_mascara(mask, L1impar);
            pondo_mascara(mask2, M1_L2impar);    
            relevantes = 7;
            break;
    }
    salvaimg(I, O, nl, nc);
   
    /*----------Processo de Afinamento da imagem-----------*/
    while (verificar) {
        for (i = 1; i < nl - 1; i++) {
            for (j = 1; j < nc - 1; j++) {
                for (k = i - 1; k < i + 2; k++) {
                    for (l = j - 1; l < j + 2; l++) {
                        if (I[k * nc + l] == mask[elemento]) {
                            cont++;
                        }
                        elemento++;
                    }
                }
                if (cont == relevantes) {
                    O[i * nc + j] = 255;
                    aux = 1;
                }
                cont = 0;
                elemento = percorreu[vezes];
            }
        }        
        vezes++;
        if (vezes == rotacao) vezes = 0;
        elemento = percorreu[vezes];
        salvaimg(O, I, nl, nc);
        
        if(rotacao == 8){
            int vezes2 = 0;
            elemento=0;
            for (i = 1; i < nl - 1; i++) {
                for (j = 1; j < nc - 1; j++) {
                    for (k = i - 1; k < i + 2; k++) {
                        for (l = j - 1; l < j + 2; l++) {
                            if (I[k * nc + l] == mask2[elemento]) {
                                cont++;
                            }
                            elemento++;
                        }
                    }
                    if (cont == relevantes) {
                        O[i * nc + j] = 255;
                        aux = 1;

                    }
                    cont = 0;
                    elemento = percorreu[vezes2];                }

            }        
            vezes2++;
            if (vezes2 == rotacao) vezes2 = 0;
            elemento = percorreu[vezes2];
            salvaimg(O, I, nl, nc);
        }
        if (aux == 0) {
            verificar = 0;
        }
        aux = 0;        
    }
  /*------------------------------------------------------------*/    
}

void msg(char *s) {
    printf("\nEsqueletizacao com afinamento homotopico\n");
    printf("\n-------------------------------");
    printf("\nUSO.:  %s  M ou L", s);
    printf("\nONDE:\n");
    printf("    <IMG.PGM>  Arquivo da imagem em formato PGM\n\n");
}

/*+------------------------------------------------------+
  |        P R O G R A M A    P R I N C I P A L          |
  +------------------------------------------------------+*/
int main(int argc, char *argv[]) {
    int OK, nc, nl, mn, tipo_img, mascara, rotacao;
    char nome[100] = "contarBolinhas.pgm", nomeSalvar[100];

    imagem In, Out;
    argc = 2;

    if (argc < 2)
        msg(argv[0]);
    else {
        OK = le_imagem_pgm(nome, &In, &nl, &nc, &mn);
        if (OK) {
            printf("\nQual máscara será utilizada?\n\n\n");
            printf("\tM1 - * 0 *\t\tM2 - * 0 0\t\tL1 - 0 0 0\t\tL2 - * 0 *\n\t     1 1 1\t\t     1 1 *\t\t     * 1 *\t\t     1 1 1\n\t     * 1 *\t\t     * 1 *\t\t     1 1 1\t\t     * 1 *");
            printf("\n\nM1 - (1)\nM2 - (2)\nL1 - (3)\nL2 - (4)\nFamilia M - (5)\nFamilia L - (6)");
            while (mascara < 1 || mascara > 6) {
                printf("\nInforme o numero correspondente da máscara: ");
                scanf("%d", &mascara);
            }
            if(mascara<5){
                while (rotacao != 4 && rotacao != 8) {
                    printf("\nInforme a quantidade de rotacoes(4 ou 8): ");
                    scanf("%d", &rotacao);
                }
            }else rotacao = 8;
            printf("\nESQUELETO DE IMAGEM");
            info_imagem(nome, nl, nc, mn);
            aloca_memo(&Out, nl, nc);
            puts("OK");
            esqueletizacao(In, Out, nl, nc, mn, mascara, rotacao);
            puts("OK");
            strcat(nome, "-nova.pgm");
            grava_imagem_pgm(Out, nome, nl, nc, mn);
            desaloca_memo(&In);
            desaloca_memo(&Out);
        }
    }
    return 0;
}